from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse

# Create your views here.

def homepage(request):
    year=1990
    month=9
    day=30
    html ="<a href='{}'>show the link</a>".format(reverse('list-url',args=(year,month,day)))
    return HttpResponse(html)


def about(request,author_no=0):
    html ="<h2> Here is no.{}'s about page".format(author_no)
    return HttpResponse(html)

def company(request):
    html ="<h2> Here is company page"
    return HttpResponse(html)

def sales(request):
    html ="<h2> Here is sales page"
    return HttpResponse(html)

def contact(request):
    html ="<h2> Here is contact page"
    return HttpResponse(html)

def listing(request,yr,mon,day):
    html ="<h2> Here is {}/{}/{} page".format(yr,mon,day)
    return HttpResponse(html)

def post(request,yr,mon,day,post_num):
    html ="<h2> Here is {}/{}/{},{} page".format(yr,mon,day,post_num)
    return HttpResponse(html)

def post2(request,yr,mon,day,post_num):
    return render(request,'post2.html',locals())