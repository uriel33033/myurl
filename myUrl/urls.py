"""myUrl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from mysite import views

my_patterns =[
    path('company/',views.company),
    path('sales/',views.sales),
    path('contact/',views.contact),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.homepage),
    path('about/',views.about), #預設值
    path('about/<int:author_no>',views.about),
    path('info/',include(my_patterns)),   #引用my_patterns路徑在info底下

    path('list/<int:yr>/<int:mon>/<int:day>/',views.listing,name='list-url'),  #多參數
    path('post/<int:yr>/<int:mon>/<int:day>/<int:post_num>',views.post),
    path('post2/<int:yr>/<int:mon>/<int:day>/<int:post_num>',views.post2,name='post-url-2')
]
